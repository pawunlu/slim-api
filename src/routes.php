<?php

use Slim\App;
use Slim\Http\Request;
use Slim\Http\Response;

return function (App $app) {
    $container = $app->getContainer();

    $app->get('/slim_[{name}]', function (Request $request, Response $response, array $args) use ($container) {
        // Sample log message
        $container->get('logger')->info("Slim-Skeleton '/' route");
        //$container->get('db')->query("select * from users");

        // Render index view
        return $container->get('renderer')->render($response, 'index.phtml', $args);
    });

    $app->get('/movies/test', 'MovieController:test');
    $app->get('/movies', 'MovieController:index');
    $app->get('/movies/{id}', 'MovieController:show');
    $app->post('/movies', 'MovieController:store');
    $app->put('/movies/{id}', 'MovieController:update');
    $app->delete('/movies/{id}', 'MovieController:destroy');
};
