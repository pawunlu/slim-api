<?php

namespace Api\Controller;

use Api\Model\Movie;

class MovieController {
    function __construct(Movie $model, \Slim\Container $container) {
        $this->model = $model;
        $this->container = $container;
    }

    function test($request, $response, $args) {
        $movies = [
            '1' => [
                'name' => 'Pulp Fiction',
                'director' => 'Quentin Tarantino'
            ],
            '2' => [
                'name' => 'Rear Window',
                'director' => 'Alfred Hitchcock'
            ]
        ];
        return $response->withJson($movies);
    }

    function show($request, $response, $args)
    {
        $movie = $this->model->select($args['id']);
        return $response->withJson($movie);
    }

    function index($request, $response, $args)
    {
        $movies = $this->model->selectAll();
        return $response->withJson($movies);
    }

    function store($request, $response, $args)
    {
        $params = $request->getParsedBody();
        $id = $this->model->insert($params);
        return $response->withJson(compact('id'));
    }

    function update($request, $response, $args)
    {
        $params = $request->getParsedBody();
        $movie = $this->model->update($args['id'], $params);
        return $response->withJson($movie);
    }

    function destroy($request, $response, $args)
    {
        $this->model->delete($args['id']);
        return $response->withJson(["status" => "OK"]);
    }
}
