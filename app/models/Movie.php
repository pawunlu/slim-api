<?php

namespace Api\Model;

/**
 *
 */
class Movie
{
    function __construct(\PDO $db)
    {
        $this->db = $db;
    }

    function select($id)
    {
        $sentencia = $this->db->prepare("SELECT * FROM movies WHERE id = :id");
        $sentencia->execute(compact('id'));
        return $sentencia->fetch();
    }

    function selectAll()
    {
        $sentencia = $this->db->prepare("SELECT * FROM movies");
        $sentencia->execute();
        return $sentencia->fetchAll();
    }

    function insert($movie)
    {
        $sql = "INSERT INTO
                    movies
                    (name, year, director, summary)
                VALUES
                    (:name, :year, :director, :summary)";
        $sentencia = $this->db->prepare($sql);
        $sentencia->execute($movie);
        return $this->db->lastInsertId();
    }

    function update($id, $new) {
        $current = $this->select($id);
        $set = "";
        foreach ($current as $field => $curval) {
            if (isset($new[$field])) {
                $current[$field] = $new[$field] ;
                $set .= " $field=:$field, " ;
            } else {
                unset($current[$field]);
            }
        }
        $current['id'] = $id;
        $set = rtrim($set, ', ');
        $where = " id = :id ";
        $sql = "UPDATE
                    movies
                SET
                    $set
                WHERE
                    $where";
        $sentencia = $this->db->prepare($sql);
        $sentencia->execute($current);
        return $this->select($id);
    }

    function delete($id)
    {
        $sql = "DELETE FROM movies WHERE id=:id";
        $sentencia = $this->db->prepare($sql);
        $sentencia->execute(compact('id'));
        return true;
    }
}
